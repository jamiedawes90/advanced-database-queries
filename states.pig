RUN load_tables.pig

state_and_places =
    JOIN state BY code,
         place BY state_code;

state_places = 
    GROUP state_and_places BY state::name;


state_places_largestfive =
    FOREACH state_places {   
        cities = 
            FILTER state_and_places
            BY type=='city';
        ordered = 
            ORDER cities
            BY population DESC;
        topfive = 
            LIMIT ordered 5;
        
       GENERATE FLATTEN(topfive);
    }


state_city_populations_topfive = 
    FOREACH state_places_largestfive
    GENERATE state::name AS state_name, place::name AS city, population;

STORE state_city_populations_topfive INTO 'TEST' USING PigStorage(',');
