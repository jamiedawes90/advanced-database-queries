SELECT state.name AS state_name, place.name
FROM state JOIN place ON state.code = place.state_code
AND place.name LIKE '%City'
AND place.type != 'city';
;

SELECT type 
FROM place 
EXCEPT 
SELECT type 
FROM mcd
;

SELECT state.name AS state_name,
       COUNT(CASE WHEN type='city' THEN 1 ELSE NULL END) AS no_city,
       COUNT(CASE WHEN type='town' THEN 1 ELSE NULL END) AS no_town,
       COUNT(CASE WHEN type='village' THEN 1 ELSE NULL END) AS no_village
FROM state JOIN place ON state.code = place.state_code
GROUP BY state.code, state.name
ORDER BY state.code
;

SELECT state.name AS name,
       SUM(county.population) AS population,
       ROUND(100*SUM(county.population)/total_population.total::numeric, 1) AS pc_population
FROM state JOIN county ON state.code = county.state_code,
     (SELECT SUM(population) AS total FROM county) total_population
GROUP BY state.code, state.name, total_population.total
ORDER BY state.code
;

SELECT big_cities.state_name AS state_name, 
       COUNT(*) AS no_big_city, 
	   SUM(big_cities.single_big_city_population) AS big_city_population
FROM (
       SELECT state.name AS state_name, 
	          place.population AS single_big_city_population
       FROM state JOIN place ON state.code = place.state_code 
	   AND place.type = 'city' AND place.population >=100000
	  ) big_cities
GROUP BY big_cities.state_name
HAVING COUNT(big_cities.*)>=5 OR SUM(big_cities.single_big_city_population) >=1000000
ORDER BY big_cities.state_name
;

SELECT state.name AS state_name, 
	   county.name AS county_name, 
	   county.population
INTO TEMP state_and_county
FROM state JOIN county ON state.code = county.state_code
;

SELECT state_name, county_name, population
FROM (
    SELECT state_and_county.state_name, 
	       state_and_county.county_name, 
		   state_and_county.population, 
	       COUNT(higher.state_name) AS rank
    FROM state_and_county JOIN state_and_county higher 
         ON state_and_county.population < higher.population 
	     AND state_and_county.state_name = higher.state_name
    GROUP BY state_and_county.state_name, 
	         state_and_county.county_name, 
			 state_and_county.population
    HAVING COUNT(higher.state_name)<=5
    ORDER BY COUNT(higher.state_name) 
      ) s
ORDER BY state_name, population DESC
;

